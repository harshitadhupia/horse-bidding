import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SplashPage } from './splash.page';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from '../login/login.component';
import { HomepageComponent } from '../homepage/homepage.component';

const routes: Routes = [
  {
    path:'',redirectTo:'home'
  },
  {
    path: '',
    component: SplashPage,
    children: [
      {
        path: 'home',
        component: HomepageComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
      path: 'sign',
      component: SignupComponent,
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class SplashPageRoutingModule {}
